This repository is a collection of solutions for the Daily Programming Challenge problems prompted by Reddit.

The goal of completing these challenges is to refine my skills in languages I have used before as well as learn new ones and emphasize in my mind how they differ from others I already know.