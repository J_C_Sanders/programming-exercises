/**
 * Created by Jason on 5/11/2017.
 *
 * Purpose of the project:
 * This is a Java solution to the first Reddit Daily Programming Challenge.
 * The program prompts the user for keyboard input of their name, age, and username,
 * then prints the data out in a sentence.
 */

import java.util.Scanner;

public class Challenge1_Easy {
    public static void main(String[] args) {
        String name;
        String age;
        String username;

        Scanner keyboard = new Scanner(System.in);

        System.out.print("Please enter your name: ");
        name = keyboard.nextLine();
        System.out.print("Please enter your age: ");
        age = keyboard.nextLine();
        System.out.print("Please enter your reddit username: ");
        username = keyboard.nextLine();

        System.out.println("Your name is " + name + ", you are " + age + " years old, and your Reddit username is "
                + username + ".");
    }
}
