/**
 * Created by Jason on 6/5/2017.
 * The purpose of this project is to implement a Caesar Cipher for single-word Strings.
 * It takes on argument from the command line in the form of the letter you'd like to shift by (ex: c -> a will shift to c)
 * It also prompts the user for input.
 */
import java.util.Scanner;

public class Challenge3_Easy {
    public static void main(String[] args) {
        Scanner key = new Scanner(System.in);
        //If the key fits the format.
        if(goodKey(args[0])) {
            String phrase = "";
            //Prompt the user for input.
            System.out.print("Enter phrase to encrypt: ");

            //Conversion only works on lowercase letters, this step is necessary to maintain accuracy in this version.
            String s = key.nextLine().toLowerCase();

            //If the input fits the format.
            if(goodInput(s)) {
                for (int i = 0; i < s.length(); i++) {
                    /* Add the ASCII value of the key to the ASCII value of every character in the String.
                     * Take that value, use modulo 97 ('a' is the 97th ASCII value, so 97 -> 0, 98 -> 1, etc is needed.)
                     * Take that value, use modulo 26 to contain input to 0-25, limits output to a-z.
                     * Take that value, add 97 to it in order to map to the correct ASCII value.
                     */
                    phrase += (char)(((((int)s.charAt(i)+((int)args[0].charAt(0)))%97)%26)+97);
                }
                System.out.println(phrase);
            }
            //Catch invalid inputs.
            else {
                System.out.println("Please use one word at a time with no numbers or special characters.");
            }
        }
        //Catch invalid keys.
        else {
            System.out.println("Please provide one letter to calculate the cipher with.");
        }
    }

    //Well-formatted keys are one alphabetical character.
    public static boolean goodKey(String s) {
        return s.matches("[a-zA-Z]");
    }

    //Well-formatted inputs are Strings containing only alphabetical characters.
    public static boolean goodInput(String s) {
        return s.matches("[a-zA-Z]+");
    }
}
