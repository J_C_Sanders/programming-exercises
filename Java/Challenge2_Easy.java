/**
 * Created by Jason on 5/25/2017.
 * The purpose of this project is to roll dice of user specification through program arguments.
 * Dice come in 2, 4, 6, 8, 10, 12, 20, and 100-sided variants.
 * Numbers can also be added in wherever the user wishes for a non-random value to be included.
 *
 * Example: 1d6+3d12+5+7d8
 * The above example will 'roll' a 6-sided die, three 12-sided dice, seven 8-sided dice, and add the results together with 5.
 *
 * This program does not have error checking - errors can be spat out if no arguments are given or the wrong format is used.
 */

import java.util.Random;

public class Challenge2_Easy {
    public static void main(String[] args) {
        //Confirm that the program is rolling what the user requested.
        System.out.println("Rolling: " + args[0]);
        int total = 0;
        //Create an array of 'rolls' to be made by splitting the argument along the '+' character.
        String[] rolls = args[0].split("\\+");
        //Add every 'roll' to the final total
        for(int i = 0; i < rolls.length; i++) {
            total += roll(rolls[i]);
        }
        System.out.println(total);
    }

    public static int roll(String r) {
        int subtotal = 0;
        Random rand = new Random();
        //Check to see if dice need to be rolled.
        if (r.contains("d")) {
            for(int i = 0; i < Integer.parseInt(r.substring(0,r.indexOf('d'))); i++) {
                int die = Integer.parseInt(r.substring(r.indexOf('d')+1));
                switch(die) {
                    case 2: subtotal += d2(rand); break;
                    case 4: subtotal += d4(rand); break;
                    case 6: subtotal += d6(rand); break;
                    case 8: subtotal += d8(rand); break;
                    case 10: subtotal += d10(rand); break;
                    case 12: subtotal += d12(rand); break;
                    case 20: subtotal += d20(rand); break;
                    case 100: subtotal += d100(rand); break;
                    default: System.out.println("This is not a supported die."); break;
                }
            }
        }
        //Just add the value if no dice are necessary.
        else {
            subtotal += Integer.parseInt(r);
        }
        return subtotal;
    }
    //Collapsed functions that return a random value based on their specific call. Stylized for simplicity.
    public static int d2(Random r) {return r.nextInt(2)+1;}
    public static int d4(Random r) {return r.nextInt(4)+1;}
    public static int d6(Random r) {return r.nextInt(6)+1;}
    public static int d8(Random r) {return r.nextInt(8)+1;}
    public static int d10(Random r) {return r.nextInt(10)+1;}
    public static int d12(Random r) {return r.nextInt(12)+1;}
    public static int d20(Random r) {return r.nextInt(20)+1;}
    public static int d100(Random r) {return r.nextInt(100)+1;}
}
