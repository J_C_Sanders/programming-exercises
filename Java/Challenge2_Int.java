/**
 * Created by Jason on 5/28/2017.
 * This project mimics the first stage of the Kingdom Hearts tutorial where players choose their character's growth style.
 * Final output will change based on previous choices.
 */
import java.util.Scanner;
public class Challenge2_Int {
    static Scanner key = new Scanner(System.in);
    public static void main(String[] args) {
        //The opening block of text with successive messages delayed for dramatic effect/readability.
        System.out.println("You awaken to find yourself in the middle of an empty room with nothing all around you");
        delay(1000);
        System.out.println("The ground rumbles beneath you and three platforms emerge from under the surface.");
        delay(1000);
        System.out.println("To the North is a sword. To the East, a staff. To the West, a shield.");
        delay(1000);
        System.out.println("Power sleeps within you. If you give it form, it will give you strength. Choose wisely.");
        delay(1000);

        System.out.print("Which pedestal will you approach: ");
        String answer = key.nextLine();

        //The first choice of the game, go n, e, or w.
        while(true) {
            //The user goes North and examines the sword.
            if (answer.toLowerCase().equals("n") || answer.toLowerCase().equals("north")) {
                System.out.println();
                System.out.println("The power of the warrior.");
                delay(1000);
                System.out.println("Invincible courage.");
                delay(1000);
                System.out.println("A sword of terrible destruction.");
                delay(1000);
                System.out.print("Is this the power you seek: ");

                String r = key.nextLine();

                //The user confirms their choice or refuses and is turned back.
                if(r.toLowerCase().equals("y") || r.toLowerCase().equals("yes")) {
                    System.out.println("Your path is set.");
                    choseSword();
                    break;
                }
                else {
                    System.out.println("Take your time.");
                    System.out.println();
                    System.out.print("Which pedestal will you approach: ");
                    answer = key.nextLine();
                }
            }
            //The user goes East and examines the staff.
            else if (answer.toLowerCase().equals("e") || answer.toLowerCase().equals("east")) {
                System.out.println();
                System.out.println("The power of the mystic.");
                delay(1000);
                System.out.println("Inner strength.");
                delay(1000);
                System.out.println("A staff of wonder and ruin.");
                delay(1000);
                System.out.print("Is this the power you seek: ");

                String r = key.nextLine();

                //The user confirms their choice or refuses and is turned back.
                if(r.toLowerCase().equals("y") || r.toLowerCase().equals("yes")) {
                    System.out.println("Your path is set.");
                    choseStaff();
                    break;
                }
                else {
                    System.out.println("Take your time.");
                    System.out.println();
                    System.out.print("Which pedestal will you approach: ");
                    answer = key.nextLine();
                }
            }
            //The user goes West and examines the shield.
            else if (answer.toLowerCase().equals("w") || answer.toLowerCase().equals("west")) {
                System.out.println();
                System.out.println("The power of the guardian.");
                delay(1000);
                System.out.println("Kindness to aid friends.");
                delay(1000);
                System.out.println("A shield to repel all.");
                delay(1000);
                System.out.print("Is this the power you seek: ");

                String r = key.nextLine();

                //The user confirms their choice or refuses and is turned back.
                if(r.toLowerCase().equals("y") || r.toLowerCase().equals("yes")) {
                    System.out.println("Your path is set.");
                    choseShield();
                    break;
                }
                else {
                    System.out.println("Take your time.");
                    System.out.println();
                    System.out.print("Which pedestal will you approach: ");
                    answer = key.nextLine();
                }
            }
            //The user goes some other direction and in turned around.
            else {
                System.out.println("This way is currently closed off.");
                System.out.println();
                System.out.print("Which pedestal will you approach: ");
                answer = key.nextLine();
            }
        }
    }
    //If the user chooses the sword, they must give up either the shield or the staff.
    public static void choseSword() {
        System.out.println();
        System.out.println("Now, what will you give up in exchange?");
        delay(1000);
        System.out.print("The shield or the staff: ");

        String s = key.nextLine();
        System.out.println();

        if(s.toLowerCase().equals("shield")) {
            System.out.println();
            System.out.println("The power of the guardian.");
            delay(1000);
            System.out.println("Kindness to aid friends.");
            delay(1000);
            System.out.println("A shield to repel all.");
            delay(1000);
            System.out.print("You give up this power: ");

            String r = key.nextLine();
            if(r.toLowerCase().equals("y") || r.toLowerCase().equals("yes")) {
                System.out.println();
                System.out.println("Your path is set.");
                delay(1000);
                System.out.println("You've gained the power of the warrior.");
                delay(1000);
                System.out.println("You've given up the power of the guardian.");
            }
            else {
                System.out.println("Take your time.");
                System.out.println();
                choseSword();
            }
        }
        else if(s.toLowerCase().equals("staff")) {
            System.out.println();
            System.out.println("The power of the mystic.");
            delay(1000);
            System.out.println("Inner strength.");
            delay(1000);
            System.out.println("A staff of wonder and ruin.");
            delay(1000);
            System.out.print("You give up this power: ");

            String r = key.nextLine();
            if(r.toLowerCase().equals("y") || r.toLowerCase().equals("yes")) {
                System.out.println();
                System.out.println("Your path is set.");
                delay(1000);
                System.out.println("You've gained the power of the warrior.");
                delay(1000);
                System.out.println("You've given up the power of the mystic.");
            }
            else {
                System.out.println("Take your time.");
                System.out.println();
                choseSword();
            }
        }
        else {
            System.out.println("That way is closed at this time.");
            choseSword();
        }
    }
    //If the user chooses the shield, they must give up either the sword or the staff.
    public static void choseShield() {
        System.out.println();
        System.out.println("Now, what will you give up in exchange?");
        delay(1000);
        System.out.print("The sword or the staff: ");

        String s = key.nextLine();

        if(s.toLowerCase().equals("sword")) {
            System.out.println();
            System.out.println("The power of the warrior.");
            delay(1000);
            System.out.println("Invincible courage.");
            delay(1000);
            System.out.println("A sword of terrible destruction.");
            delay(1000);
            System.out.print("You give up this power: ");

            String r = key.nextLine();
            if(r.toLowerCase().equals("y") || r.toLowerCase().equals("yes")) {
                System.out.println();
                System.out.println("Your path is set.");
                delay(1000);
                System.out.println("You've gained the power of the guardian.");
                delay(1000);
                System.out.println("You've given up the power of the warrior.");
            }
            else {
                System.out.println("Take your time.");
                System.out.println();
                choseShield();
            }
        }
        else if(s.toLowerCase().equals("staff")) {
            System.out.println();
            System.out.println("The power of the mystic.");
            delay(1000);
            System.out.println("Inner strength.");
            delay(1000);
            System.out.println("A staff of wonder and ruin.");
            delay(1000);
            System.out.print("You give up this power: ");

            String r = key.nextLine();
            if(r.toLowerCase().equals("y") || r.toLowerCase().equals("yes")) {
                System.out.println();
                System.out.println("Your path is set.");
                delay(1000);
                System.out.println("You've gained the power of the guardian.");
                delay(1000);
                System.out.println("You've given up the power of the mystic.");
            }
            else {
                System.out.println("Take your time.");
                System.out.println();
                choseShield();
            }
        }
        else {
            System.out.println("That way is closed at this time.");
            choseShield();
        }
    }
    //If the user chooses the staff, they must give up either the sword or the shield.
    public static void choseStaff() {
        System.out.println();
        System.out.println("Now, what will you give up in exchange?");
        delay(1000);
        System.out.print("The sword or the shield: ");

        String s = key.nextLine();

        if(s.toLowerCase().equals("sword")) {
            System.out.println();
            System.out.println("The power of the warrior.");
            delay(1000);
            System.out.println("Invincible courage.");
            delay(1000);
            System.out.println("A sword of terrible destruction.");
            delay(1000);
            System.out.print("You give up this power: ");

            String r = key.nextLine();
            if(r.toLowerCase().equals("y") || r.toLowerCase().equals("yes")) {
                System.out.println();
                System.out.println("Your path is set.");
                delay(1000);
                System.out.println("You've gained the power of the mystic.");
                delay(1000);
                System.out.println("You've given up the power of the warrior.");
            }
            else {
                System.out.println("Take your time.");
                System.out.println();
                choseStaff();
            }
        }
        else if(s.toLowerCase().equals("shield")) {
            System.out.println();
            System.out.println("The power of the guardian.");
            delay(1000);
            System.out.println("Kindness to aid friends.");
            delay(1000);
            System.out.println("A shield to repel all.");
            delay(1000);
            System.out.print("You give up this power: ");

            String r = key.nextLine();
            if(r.toLowerCase().equals("y") || r.toLowerCase().equals("yes")) {
                System.out.println();
                System.out.println("Your path is set.");
                delay(1000);
                System.out.println("You've gained the power of the mystic.");
                delay(1000);
                System.out.println("You've given up the power of the guardian.");
            }
            else {
                System.out.println("Take your time.");
                System.out.println();
                choseStaff();
            }
        }
        else {
            System.out.println("That way is closed at this time.");
            choseStaff();
        }
    }
    //Simplification of the delay method to aid in programming speed - calling delay is easier than calling Thread.sleep.
    public static void delay(int ms) {
        try {
            Thread.sleep(ms);
        }
        catch(InterruptedException e) {}
    }
}