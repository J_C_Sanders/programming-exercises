/**
 * Created by Jason on 5/13/2017.
 *
 * The purpose of this project is to have the computer play a guessing game with the user.
 * The user picks a number between 1 and 100, and the computer begins guessing.
 * The program will have the computer guess and adjust based on user response of High or Low until it succeeds.
 * Unless the user lies, the program will always guess correctly in no more than 7 attempts.
 */

import java.util.Scanner;

public class Challenge1_Hard {
    public static void main(String[] args) {
        //Start with a highest possible value of 101 and a lowest possible value of 1: Guess 50.
        int high = 101;
        int low = 1;
        int guess = 50;
        System.out.println("Let's play a guessing game. Pick a number between 1 and 100.");
        System.out.println("Respond with [Y]es, [H]igh, or [L]ow.");
        //Recursively guess following the same pattern.
        guessLoop(high, low, guess);
    }
    //Take the average of your high and low value, then guess that.
    public static void guessLoop(int h, int l, int g) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Is " + g + " your number? ");
        String input = keyboard.nextLine();

        //Celebrate when the computer guesses correctly, then stop the run.
        if(input.toUpperCase().equals("Y")) {
            System.out.println("Ha! I've got your number now!");
            keyboard.close();
        }
        //If too low, make the current guess the new low and recalculate the guess. Guess again.
        else if (input.toUpperCase().equals("L")) {
            l = g;
            g = (h+l)/2;
            guessLoop(h, l, g);
        }
        //If too high, make the current guess the new high and recalculate the guess. Guess again.
        else if (input.toUpperCase().equals("H")) {
            h = g;
            g = (h+l)/2;
            guessLoop(h, l, g);
        }
        //If the user enters anything besides Y, y, H, h, L, or l, remind them of the rules.
        else {
            System.out.println("Please use [Y]es, [H]igh, or [L]ow.");
            guessLoop(h, l, g);
        }
    }
}
