/**
 * Created by Jason on 6/16/2017.
 */
import java.util.Scanner;
import java.util.ArrayList;

public class Challenge2_Hard {
    public static void main(String[] args) {
        Scanner key = new Scanner(System.in);
        ArrayList<Double> laps = new ArrayList<Double>();
        final Double start = (double)System.currentTimeMillis();
        char command = 'q';
        while(command != 's') {
            System.out.print("[L]ap or [S]top: ");
            try {
                command = key.nextLine().toLowerCase().charAt(0);
            }
            catch (IndexOutOfBoundsException e) { }
            if (command == 'l') {
                laps.add(((double) System.currentTimeMillis() - start) / 1000.0);
            }
        }
        laps.add(((double)System.currentTimeMillis() - start)/1000.0);
        System.out.println(laps);
    }
}
