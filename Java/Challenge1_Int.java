/**
 * Created by Jason on 5/12/2017.
 * The purpose of this project is to create a menu-driven planner that lets you add, delete, and view tasks by hour.
 * This project uses ArrayLists to support dynamic array sizes in case a user wants to add, cancel, and overbook events.
 *
 * ¯\_(ツ)_/¯
 */

import java.util.Scanner;
import java.util.ArrayList;

public class Challenge1_Int {

    //Create a new ArrayList composed of ArrayLists that contain strings for events.
    private static ArrayList<ArrayList<String>> schedule = new ArrayList<ArrayList<String>>();
    private static Scanner keyboard = new Scanner(System.in);
    public static void main(String args[]) {
        for (int i = 0; i < 24; i++) {
            schedule.add(new ArrayList<String>());
        }
        System.out.println("Welcome to your schedule.");
        menu();
    }

    public static void menu() {
        //Use the boolean to run a continuous loop for the main menu.
        boolean menuSelect = false;

        while(!menuSelect) {
            //To scan for proper format of user input, utilize a try/catch statement
            try {
                System.out.println("1) Add\n2) Delete\n3) View\n4) Exit");
                int action = Integer.parseInt(keyboard.nextLine());
                switch (action) {
                    case 1:
                        add();
                        break;
                    case 2:
                        delete();
                        break;
                    case 3:
                        view();
                        break;
                    case 4:
                        System.out.println("Closing application...");
                        menuSelect = true;
                        System.exit(0);
                    default:
                        System.out.println("Please use a number between 1 and 4");
                        break;
                }
            }
            //Catch the error where the user attempts to input a string.
            catch(NumberFormatException nfe) {
                System.out.println("Please use digits.");
            }
        }
    }
    //Add an event to the schedule.
    public static void add() {
        Scanner addBoard = new Scanner(System.in);
        int hour;
        //Continue to take user input until an accurate hour is given.
        while(true) {
            try {
                System.out.print("Add to which hour (0 - 23): ");
                hour = Integer.parseInt(addBoard.nextLine());
                if(hour > -1 & hour < 24) {
                    break;
                }
                else {
                    System.out.println("Please use a number between 0 and 23.");
                }
            }
            //Catch the error where the user inputs a String.
            catch(NumberFormatException nfe) {
                System.out.println("Please use digits.");
            }
        }
        //Request a name for the event, then return to the main menu.
        System.out.print("Name the event: ");
        String event = addBoard.nextLine();
        schedule.get(hour).add(event);
        System.out.println(event + " added to schedule");
        menu();
    }
    //Delete an event from the schedule.
    public static void delete() {
        Scanner delBoard = new Scanner(System.in);
        int hour;
        //Use a while loop to continuously take input until the user provides a proper hour.
        while(true) {
            try {
                System.out.print("Delete from which hour? (0 - 23): ");
                hour = Integer.parseInt(delBoard.nextLine());
                if (hour > -1 & hour < 24) {
                    break;
                }
                else{
                    System.out.println("Please use an hour between 0 and 23");
                }
            }
            //Stop the user from using Strings instead of ints.
            catch(NumberFormatException nfe) {
                System.out.println("Please use digits.");
            }
        }
        //Make sure there are events to delete. If not, redirect the user to the menu.
        if(schedule.get(hour).size() == 0) {
            System.out.println("No appointments at this time.");
            menu();
        }
        //Print the list of events in the hour, then prompt the user to select an index for an event.
        System.out.println(schedule.get(hour));
        System.out.print("Index of event (0 - " + (schedule.get(hour).size()) + "): ");
        //Take user input until an event is deleted.
        while(true) {
            try {
                int event = Integer.parseInt(delBoard.nextLine());
                System.out.println(event);
                schedule.get(hour).remove(event);
                //Make sure that the user enters a valid event index.
                if (event > -1 & event <= schedule.get(hour).size()) {
                    System.out.println("Event removed.");
                    menu();
                }
                else {
                    System.out.println("Please choose an index within the range of events.");
                }
            }
            //Stop the user from entering Strings instead of ints.
            catch (NumberFormatException nfe) {
                System.out.println("Please use digits.");
            }
        }
    }
    //Print out the schedule with hours separated by square braces.
    public static void view() {
        System.out.println(schedule);
        menu();
    }
}
