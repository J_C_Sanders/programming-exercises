# Created by Jason on 5/18/17
# This program will roll regulation d20 dice as specified by a string from the command line.
# ex: 1d6+2d12+5: Will roll 1 six-sided die, 2 twelve-sided dice, and add five to the result.
#
# This program does not contain proper error checking. Arguments of the incorrect format will return errors.

import random, sys
global total
total = 0
args = sys.argv

# Given a number of one type of die to roll, roll that number.
def roll(r):
    global total
    if 'd' in r:
        # This is the part where you hate that Python doesn't have switch statements.
        # After obtaining a substring of the format 'XdY', the program rolls die-type Y a total of X times.
        for j in range(int(r.split('d')[0])):
            if int(r.split('d')[1]) is 2:
                total += d2()
            elif int(r.split('d')[1]) is 4:
                total += d4()
            elif int(r.split('d')[1]) is 6:
                total += d6()
            elif int(r.split('d')[1]) is 8:
                total += d8()
            elif int(r.split('d')[1]) is 10:
                total += d10()
            elif int(r.split('d')[1]) is 12:
                total += d12()
            elif int(r.split('d')[1]) is 20:
                total += d20()
            elif int(r.split('d')[1]) is 100:
                total += d100()
            else:
                print("invalid die used")
    # If the substring is an int, add it to the total without rolling.
    else:
        total += int(r)


# Return random numbers based on the type of die rolled.
def d2():
    return random.randint(1, 2)
def d4():
    return random.randint(1, 4)
def d6():
    return random.randint(1, 6)
def d8():
    return random.randint(1, 8)
def d10():
    return random.randint(1, 10)
def d12():
    return random.randint(1, 12)
def d20():
    return random.randint(1, 20)
def d100():
    return random.randint(1, 100)

# Check for no arguments,
if args is "":
    print("No arguments given")
else:
    print(args[1])
    rollList = args[1].split('+')
    for i in range(len(rollList)):
        roll(rollList[i])

    # Print the final value of all rolled dice and added constants.
    print(total)
