import time

lap = [];
input("Press enter to start the stopwatch.")
start = time.time()
command = input("[L]ap or [S]top: ")
while command.lower() != 's':
    if command.lower() == 'l':
        lap.append(str(time.time() - start)[:4])
        command = input("[L]ap or [S]top: ")
    else:
        command = input("[L]ap or [S]top: ")

stop = str(time.time() - start)[:4]
lap.append(stop)
print(lap)
