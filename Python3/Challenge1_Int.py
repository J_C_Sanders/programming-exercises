# Created by Jason on 5/11/2017
#
# The purpose of this project is to create a chronological list of events that can be added to or subtracted from.


schedule = []
# Create a list of 24 Lists, one list for each hour.
for i in range(24):
    schedule.append([])


# Implement the main menu and navigation through it.
def main():
    print("Add, Delete, View, or Exit?")
    task = input()

    if task.lower() == "add":
        add()
    elif task.lower() == "delete":
        delete()
    elif task.lower() == "view":
        view()
    elif task.lower() == "exit":
        print("Closing application...")
    else:
        print("Invalid input. Try again.")
        main()

# Add an event to the user's schedule.
def add():
    add_hour = 24
    # Make sure a valid hour has been entered.
    while add_hour > 23 or add_hour < 0:
        try:
            add_hour = int(input("Which hour to add an event to (0 - 23): "))
            if add_hour > 23 or add_hour < 0:
                print("Please use and integer between 0 and 23.")
                add_hour = 24
        except ValueError:
            print("Please use an integer between 0 and 23.")

    # Give the event a name and add it to the appropriate list.
    event = input("Name of the event: ")
    schedule[add_hour].append(event)
    print(event + " has been added to the schedule")
    main()

# Delete an event from the schedule.
def delete():
    del_hour = 24
    # Make sure a valid hour is being used.
    while del_hour > 23:
        try:
            del_hour = int(input("Which hour to add an event to (0 - 23): "))
            if del_hour > 23 or del_hour < 0:
                print("Please use an integer between 0 and 23.")
                del_hour = 24
        except ValueError:
            print("Please use an integer.")

    # Make sure there is an event to be deleted at all.
    if len(schedule[del_hour]) is 0:
        print("No items at this time.")
        main()

    # Print the selected hour and have the user choose which index to delete.
    print(schedule[del_hour])
    eventID = len(schedule[del_hour])+1
    while eventID not in range(len(schedule[del_hour])):
        try:
            eventID = int(input("Which event to remove (0 - " + str(len(schedule[del_hour])-1) + "): "))
            if eventID > len(schedule[del_hour]):
                print("Please use an integer between 0 and " + str(len(schedule[del_hour])-1))
        except ValueError:
            print("Please use an integer.")

    del schedule[del_hour][eventID]
    print("Event deleted")
    main()

# Print the entire schedule
def view():
    for k in range(len(schedule)):
        print(str(k) + ": " + str(schedule[k]))
    main()

print("Welcome to your schedule.")
main()
