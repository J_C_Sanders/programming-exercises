# The following program will guide the user through a text adventure adaptation of the Kingdom Hearts introduction stage
# The user will be asked to pick between a sword, staff, and shield, and then give up one of the two they did not select
# The user will then answer three multiple choice questions
#
# Final output will vary based on choices made


# The initial text of the game; spaced out by user input for dramatic effect/readability.
input("You awaken to find yourself in the middle of a room with nothing all around you.")
input("The ground rumbles beneath you and three platforms emerge from under the surface.")
input("To the North is a sword. To the East, a staff. To the West, a shield.")
input("Power sleeps within you. If you give it form, it will give you strength. Choose wisely.")


# The user's first choice of the game.
def hub1():
    direction = input("The three pedestals lie before you. Which path will you take: ")
    # The user approaches and examines the sword.
    if direction.lower() == "n" or direction.lower() == "north":
        print()
        input("The power of the warrior.")
        input("Invincible courage.")
        input("A sword of terrible destruction.")
        answer = input("Is this the power you seek: ")

        # The user confirms their choice or is turned back.
        if answer.lower() == "y" or answer.lower() == "yes":
            gain = "warrior"
            input("Your path is set.")
            hub2(gain)
        else:
            input("Take your time.")
            hub1()

    # The user approaches and examines the staff.
    elif direction.lower() == "e" or direction.lower() == "east":
        print()
        input("The power of the mystic.")
        input("Inner strength.")
        input("A staff of wonder and ruin.")
        answer = input("Is this the power you seek: ")

        # The user confirms their choice or is turned back.
        if answer.lower() == "y" or answer.lower() == "yes":
            gain = "mystic"
            input("Your path is set.")
            hub2(gain)
        else:
            input("Take your time.")
            hub1()

    # The user approaches and examines the shield.
    elif direction.lower() == "w" or direction.lower() == "west":
        print()
        input("The power of the guardian.")
        input("Kindness to aid friends.")
        input("A shield to repel all.")
        answer = input("Is this the power you seek: ")

        # The user confirms their choice or is turned back.
        if answer.lower() == "y" or answer.lower() == "yes":
            gain = "guardian"
            input("Your path is set.")
            hub2(gain)
        else:
            input("Take your time.")
            hub1()

    # The user goes in some other direction and is turned back.
    else:
        input("That way is currently closed to you.")
        hub1()


# Provide the user with a second choice based on their first.
def hub2(gain):
    print()
    input("Now, what will you give up in exchange?")

    # Give up one of the two items you haven't chosen.
    if gain == "warrior":
        answer = input("The staff or the shield: ")
    elif gain == "guardian":
        answer = input("The sword or the staff: ")
    else:
        answer = input("The shield or the sword: ")

    # The user approaches and examines the staff.
    if answer.lower() == "staff" and gain != "staff":
        print()
        input("The power of the mystic.")
        input("Inner strength.")
        input("A staff of wonder and ruin.")
        choice = input("You give up this power: ")

        # The user confirmes their choice or is turned back.
        if choice.lower() == "y" or choice.lower() == "yes":
            print()
            input("You've gained the power of the " + gain + ".")
            input("You gave up the power of the mystic.")
            exit()
        else:
            print()
            input("Take your time.")
            hub2(gain)

    # The user approaches and examines the sword.
    elif answer.lower() == "sword" and gain != "sword":
        print()
        input("The power of the warrior.")
        input("Invincible courage.")
        input("A sword of terrible destruction.")
        choice = input("You give up this power: ")

        # The user confirms their choice or is turned back.
        if choice.lower() == "y" or choice.lower() == "yes":
            print()
            input("You gain the power of the " + gain + ".")
            input("You give up the power of the warrior.")
            exit()
        else:
            print()
            input("Take your time.")
            hub2(gain)

    # The user approaches and examines the shield.
    elif answer.lower() == "shield" and gain != "shield":
        print()
        input("The power of the guardian.")
        input("Kindness to aid friends.")
        input("A shield to repel all.")
        choice = input("You give up this power: ")

        # The user confirms their choice or is turned back.
        if choice.lower() == "y" or choice.lower() == "yes":
            print()
            input("You gain the power of the " + gain + ".")
            input("You give up the power of the guardian.")
            exit()
        else:
            print()
            input("Take your time.")
            hub2(gain)

    # The user tries to pick something that isn't available.
    else:
        print("That road is not open to you at this time.")
        hub2(gain)

# Give the user their first choice.
hub1()
