# Created by Jason on 5/11/2017
#
# Purpose of the project:
# This is a Python solution to the first Reddit Daily Programming Challenge.
# The program prompts the user for keyboard input of their name, age, and username,
# then prints the data out in a sentence.

def main():
    name = input("Please enter your name: ");
    age = input("Please enter your age: ");
    username = input("Please enter your Reddit username: ");

    print("Your name is " + name + ", you are " + age + " years old, and your Reddit username is " + username + ".");

main();