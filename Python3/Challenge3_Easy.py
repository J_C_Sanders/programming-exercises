# Created by Jason on 6/5/17
# The following program will take user input and encrypt it with a Caesar Cipher determined from the command line.

import sys
args = sys.argv

# Use the Cryptographic key 'c' to encode user-input String 's'.
def encrypt(c):
    s = input("Please input the phrase to encrypt: ").lower()
    # Check to see if the entered phrase is alphabetical - this prevents multi-word cryptography at present
    # TODO: Check to see if the user input contains spaces and split the input into an array along those spaces.
    # TODO: Funnel each element of that array through this encryption process.
    if s.isalpha():
        phrase = ""
        for i in range(len(s)):
            phrase += chr((((ord(s[i])+ord(c))%97)%26)+97)
        print()
        return phrase
    # Catch invalid inputs
    else:
        return "Please use one word at a time with no numbers or special characters."

# Check that the key is valid before starting.
if args[1].isalpha() and len(args[1]) is 1:
    print("Encrypted message: " + encrypt(args[1].lower()))
# Reject invalid keys.
else:
    print("Please provide one letter to calculate the cipher with.")
