# Created by Jason on 5/13/17
#
# The purpose of this assignment is to create a computer program that can guess a number between 1 and 100.
# Unless the user lies, the program will always guess correctly within 7 attempts.

# Set the lowest and highest possible values for the number, provide the first guess, and begin the guessing loop.
def main():
    low = 0
    high = 101
    guess = 50
    print("Welcome to the guessing game. Please respond with [L]ow, [H]igh, or [Y]es.")
    response = input("Is " + str(guess) + " your number: ")

    while response.lower() != "y":
        # If too low, make the guess the new low and calculate a new guess (average of high and low values)
        if response.lower() == "l":
            low = guess
            guess = round((low+high)/2)
            response = input("Is " + str(guess) + " your number: ")
        # If too high, make the guess the new high and calculate a new guess (average of high and low values)
        elif response.lower() == "h":
            high = guess
            guess = round((low+high)/2)
            response = input("Is " + str(guess) + " your number: ")
        # Catch invalid inputs.
        else:
            print("Please use [L]ow, [H]igh, or [Y]es.")
            response = input("Is " + str(guess) + " your number: ")

    # Celebrate if the program is correct.
    print("Ha! I've got your number now!")


main()
