/**
  * Created by Jason on 5/12/2017.
  */

object Challenge1_Easy {
  def main(args: Array[String]) {
    val name = scala.io.StdIn.readLine("Please give your name: ");
    val age = scala.io.StdIn.readLine("Please give your age: ");
    val username = scala.io.StdIn.readLine("Please give your Reddit username: ");

    println("Your name is " + name + ", you are " + age + " years old, and your Reddit username is " + username + ".");
  }
}
