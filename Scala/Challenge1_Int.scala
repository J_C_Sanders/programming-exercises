/**
  * Created by Jason on 5/12/2017.
  * The purpose of this project is to create a menu-driven planner that lets the user add, remove, and view events organized by hour.
  */

import scala.collection.mutable.ArrayBuffer

object Challenge1_Int {
  val reader = scala.io.StdIn
  //Use ArrayBuffers for variable sizes of lists. This creates flexible schedules.
  val schedule: ArrayBuffer[ArrayBuffer[String]] = new ArrayBuffer[ArrayBuffer[String]]

  def main(args: Array[String]) {
    //Create a new ArrayBuffer for each hour in the day.
    for (i <- 0 to 23) {
      schedule.append(new ArrayBuffer[String])
    }
    println("Welcome to your schedule.")
    println(take_input())
  }

  //Take input from the user for the main menu.
  def take_input(): String = {
    val input = reader.readLine("1)View\n2)Add\n3)Delete\n4)Exit\n")
    input match {

      case "1" =>
        println(view())
        take_input()
        ""

      case "2" =>
        println(add())
        take_input()
        ""

      case "3" =>
        println(delete())
        take_input()
        ""

      case "4" => "Closing program..."

      case _ =>
        println("Please use numbers 1 to 4.")
        take_input()
        ""
    }
  }

  //Print the entire schedule.
  def view(): String = {
    for (i <- 0 to 23) {
      print(i.toString() + ": ")
      println(schedule(i))
    }
    ""
  }

  //Add an event to the schedule.
  def add(): String = {
    //Get an hour and an event name.
    try {
      print("Add to which hour: ")
      val hour: Int = reader.readInt()
      val event = reader.readLine("Name the event: ")
      schedule(hour).append(event)
    }
    //Catch invalid input.
    catch {
      case e: Exception =>
        println("Invalid input.")
        add()
    }
    //Confirmation message.
    "Added event to schedule."
  }

  //Delete an event from the schedule.
  def delete(): String = {
    //Get an hour to delete from and an index to delete.
    try {
      print("Delete from which hour: ")
      val hour = reader.readInt()
      println(schedule(hour))
      print("Delete which event (0,1,...): ")
      val index = reader.readInt()
      schedule(hour).remove(index)
    }
    catch {
      case e: Exception =>
        println("Invalid input.")
        delete()
    }
    "Removed event from schedule."
  }
}