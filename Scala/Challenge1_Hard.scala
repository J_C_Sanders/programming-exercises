/**
  * Created by Jason on 5/13/2017.
  *
  * This program will play a guessing game with the user.
  * The user picks between 1 and 100, and the computer will guess it within 7 attempts.
  */
object Challenge1_Hard {
  //Set the high and low values and the first guess. Start the loop
  def main(args: Array[String]) {
    val high = 101
    val low = 1
    val guessNum = 50
    println("Let's play a guessing game. Pick a number between 1 and 100.")
    println("Use [Y]es, [H]igh, or [L]ow.")
    //Start the loop for guessing.
    guessLoop(high, low, guessNum)
  }
  def guessLoop(h: Int, l: Int, g: Int) {
    val input = scala.io.StdIn.readLine(s"Is $g your number? ")
    //If the guess is correct, celebrate.
    if(input.equals("Y")){
      println("Ha! I've got your number now")
    }
    //If the guess is too high, run again with the guess as the new high and a new guess (the average of the high and low.)
    else if(input.equals("H")){
      guessLoop(g, l, (g+l)/2)
    }
    //If the guess is too low, run again with the guess as the new low and a new guess (the average of the high and low.)
    else if(input.equals("L")){
      guessLoop(h, g, (h+g)/2)
    }
    //If the input is invalid, remind the user of the rules.
    else {
      println("Please use [Y]es, [H]igh, or [L]ow.")
      guessLoop(h, l, g)
    }
  }
}
