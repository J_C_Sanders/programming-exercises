/**
  * Created by Jason on 6/20/2017.
  */
import scala.collection.mutable.ArrayBuffer

object Challenge2_Hard {
  def main(args: Array[String]): Unit = {
    val times = new ArrayBuffer[String]
    val reader = scala.io.StdIn

    reader.readLine("Hit enter to start the clock")
    val start = now()
    var cmd = reader.readLine("[L]ap or [S]top: ")
    while (!cmd.toLowerCase().equals("s")) {
      if(cmd.toLowerCase().equals("l")) {
        val strNow = ((now()-start)/1000.0).toString().substring(0,4)
        times.append(strNow)
        cmd = reader.readLine("[L]ap or [S]top: ")
      }
      else {
        cmd = reader.readLine("[L]ap or [S]top: ")
      }
    }
    times.append(((now()-start)/1000.0).toString().substring(0,4))
    print(times)
  }
  def now(): Double = System.currentTimeMillis()
}
