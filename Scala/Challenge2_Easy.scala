/**
  * Created by Jason on 5/17/2017.
  * The purpose of this project is to create a calculator that performs several useful functions.
  * AKA rolls dice for when I play D&D and need to roll anywhere form one to twenty d6's.
  * This will take in a string from the command line in the format of '1d12+3d6+1d4+8' and calculate the total roll.
  *
  * This program does not have proper error checking. It will return errors if the arguments do not match the format.
  */
object Challenge2_Easy {
  val r = scala.util.Random
  def main(args: Array[String]) {
    var result = 0
    try {
      //The program confirms that it will try to roll the user's dice.
      println("Rolling " + args(0))
      //Split the argument into an array of substrings by the '+' character.
      val rollList = args(0).split('+')
      if (rollList.length > 0) {
        for (i <- 0 until rollList.length) {
          //Roll dice if they need to be rolled...
          if (rollList(i).contains('d')) {
            val dice = rollList(i).split('d')
            dice(1) match {
              case "2" => for (k <- 1 to dice(0).toInt) {result += d2()}
              case "4" => for (k <- 1 to dice(0).toInt) {result += d4()}
              case "6" => for (k <- 1 to dice(0).toInt) {result += d6()}
              case "8" => for (k <- 1 to dice(0).toInt) {result += d8()}
              case "10" => for (k <- 1 to dice(0).toInt) {result += d10()}
              case "12" => for (k <- 1 to dice(0).toInt) {result += d12()}
              case "20" => for (k <- 1 to dice(0).toInt) {result += d20()}
              case "100" =>  for (k <- 1 to dice(0).toInt) {result += d100()}
              case _ => println("Invalid Input")
            }
          }
          //...Otherwise just add the value to the end.
          else {
            result += rollList(i).toInt
          }
        }
      }
    }
    //Report when no arguments have been provided.
    catch {
      case aioob: ArrayIndexOutOfBoundsException => println("No arguments provided")
    }
    finally {
      println(result)
    }
  }
  //This is where we can make use of Scala's ability to be a functional programming language.
  def d2(): Int = r.nextInt(1)+1
  def d4(): Int = r.nextInt(3)+1
  def d6(): Int = r.nextInt(5)+1
  def d8(): Int = r.nextInt(7)+1
  def d10(): Int = r.nextInt(9)+1
  def d12(): Int = r.nextInt(11)+1
  def d20(): Int = r.nextInt(19)+1
  def d100(): Int = r.nextInt(99)+1
}