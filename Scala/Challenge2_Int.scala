/**
  * Created by Jason on 6/2/2017.
  *
  * This program mimics the introduction to Kingdom Hearts where users are given a series of choices.
  * Ending output will change based on the choices users have made.
  */
object Challenge2_Int {
  val reader = scala.io.StdIn
  def main(args: Array[String]) {
    //Starting text delayed behind user input for readability and dramatic effect.
    reader.readLine("You awaken in the middle of a room with nothing all around")
    reader.readLine("The ground rumbles beneath you and three platforms emerge from under the surface.")
    reader.readLine("To the North is a sword. To the East, a staff. To the West, a shield.")
    reader.readLine("Power sleeps within you. If you give it form, it will give you strength. Choose wisely.")
    firstChoice()
  }

  //The user makes their first choice.
  def firstChoice() {
    val answer: String = reader.readLine("Which pedestal will you approach: ")
    //The user approaches and inspects the sword.
    if(answer.toLowerCase().equals("n") || answer.toLowerCase().equals("north")) {
      print()
      reader.readLine("The power of the warrior.")
      reader.readLine("Invincible courage.")
      reader.readLine("A sword of terrible destruction.")
      val a = reader.readLine("Is this the power you seek: ")

      //The user confirms their choice or turns back.
      if(a.toLowerCase().equals("y") || a.toLowerCase().equals("yes")) {
        print()
        reader.readLine("Your path is set.")
        choseSword()
      }
      else {
        print()
        reader.readLine("Take your time.")
        firstChoice()
      }
    }
    //The user approaches and examines the staff.
    else if(answer.toLowerCase().equals("e") || answer.toLowerCase().equals("east")) {
      print()
      reader.readLine("The power of the mystic.")
      reader.readLine("Inner strength.")
      reader.readLine("A staff of wonder and ruin.")
      val a = reader.readLine("Is this the power you seek: ")

      //The user confirms their choice or turns back.
      if(a.toLowerCase().equals("y") || a.toLowerCase().equals("yes")) {
        print()
        reader.readLine("Your path is set.")
        choseSword()
      }
      else {
        print()
        reader.readLine("Take your time.")
        firstChoice()
      }
    }
    //The user approaches and inspects the shield.
    else if(answer.toLowerCase().equals("w") || answer.toLowerCase().equals("west")) {
      print()
      reader.readLine("The power of the guardian.")
      reader.readLine("Kindness to help friends.")
      reader.readLine("A shield that repels all.")
      val a = reader.readLine("Is this the power you seek: ")

      //The user confirms their choice or turns back.
      if (a.toLowerCase().equals("y") || a.toLowerCase().equals("yes")) {
        print()
        reader.readLine("Your path is set.")
        choseSword()
      }
      else {
        print()
        reader.readLine("Take your time.")
        firstChoice()
      }
    }
    //The user tries to go in some invalid direction.
    else {
      print("That way is closed off to you for now.")
      print()
      firstChoice()
    }
  }

  //The user chooses the sword and must give up the shield or the staff.
  def choseSword() {
    print()
    val r: String = reader.readLine("Now, what will you give up in exchange?\nThe shield or the staff: ")
    //The user approaches and inspects the staff.
    if(r.toLowerCase().equals("staff")) {
      print()
      reader.readLine("The power of the mystic.")
      reader.readLine("Inner strength.")
      reader.readLine("A staff of wonder and ruin.")
      val a = reader.readLine("You give up this power: ")

      //The user gives up the staff or turns back.
      if(a.toLowerCase().equals("y") || a.toLowerCase().equals("yes")) {
        print()
        reader.readLine("Your path is set.")
        reader.readLine("You've gained the power of the warrior.")
        reader.readLine("You've given up the power of the mystic.")
      }
      else {
        print()
        reader.readLine("Take your time.")
        choseSword()
      }
    }
    //The user approaches and inspects the shield.
    else if(r.toLowerCase().equals("shield")) {
      print()
      reader.readLine("The power of the guardian.")
      reader.readLine("Kindness to help friends.")
      reader.readLine("A shield that repels all.")
      val a = reader.readLine("You give up this power: ")

      //The user gives up the shield or turns back.
      if(a.toLowerCase().equals("y") || a.toLowerCase().equals("yes")) {
        print()
        reader.readLine("Your path is set.")
        reader.readLine("You've gained the power of the warrior.")
        reader.readLine("You've given up the power of the guardian.")
      }
      else {
        print()
        reader.readLine("Take your time.")
        choseSword()
      }
    }
    //The user goes in some invalid direction.
    else {
      print("That way is closed off to you for now.")
      choseSword()
    }
  }

  //The user chose the shield and must give up the sword or the staff.
  def choseShield() {
    print()
    val r: String = reader.readLine("Now, what will you give up in exchange?\nThe sword or the staff: ")
    //The user approaches and inspects the staff.
    if(r.toLowerCase().equals("staff")) {
      print()
      reader.readLine("The power of the mystic.")
      reader.readLine("Inner strength.")
      reader.readLine("A staff of wonder and ruin.")
      val a = reader.readLine("You give up this power: ")

      //The user gives up the staff or turns back.
      if(a.toLowerCase().equals("y") || a.toLowerCase().equals("yes")) {
        print()
        reader.readLine("Your path is set.")
        reader.readLine("You've gained the power of the guardian.")
        reader.readLine("You've given up the power of the mystic.")
      }
      else {
        print()
        reader.readLine("Take your time.")
        choseShield()
      }
    }
    //The user approaches and inspects the sword.
    else if(r.toLowerCase().equals("sword")) {
      print()
      reader.readLine("The power of the warrior.")
      reader.readLine("Invincible courage.")
      reader.readLine("A sword of terrible destruction.")
      val a = reader.readLine("You give up this power: ")

      //The user gives up the sword or turns back.
      if(a.toLowerCase().equals("y") || a.toLowerCase().equals("yes")) {
        print()
        reader.readLine("Your path is set.")
        reader.readLine("You've gained the power of the guardian.")
        reader.readLine("You've given up the power of the warrior.")
      }
      else {
        print()
        reader.readLine("Take your time.")
        choseShield()
      }
    }
    //The user goes in some other direction.
    else {
      print("That way is closed off to you for now.")
      choseShield()
    }
  }

  //The user chose the staff and must give up the sword or the shield.
  def choseStaff() {
    print()
    val r: String = reader.readLine("Now, what will you give up in exchange?\nThe shield or the sword: ")
    //The user approaches and inspects the sword.
    if(r.toLowerCase().equals("sword")) {
      print()
      reader.readLine("The power of the warrior.")
      reader.readLine("Invincible courage.")
      reader.readLine("A sword of terrible destruction.")
      val a = reader.readLine("You give up this power: ")

      //The user gives up the sword or turns back.
      if(a.toLowerCase().equals("y") || a.toLowerCase().equals("yes")) {
        print()
        reader.readLine("Your path is set.")
        reader.readLine("You've gained the power of the mystic.")
        reader.readLine("You've given up the power of the warrior.")
      }
      else {
        print()
        reader.readLine("Take your time.")
        choseStaff()
      }
    }
    //The user approaches and inspects the shield.
    else if(r.toLowerCase().equals("shield")) {
      print()
      reader.readLine("The power of the guardian.")
      reader.readLine("Kindness to help friends.")
      reader.readLine("A shield that repels all.")
      val a = reader.readLine("You give up this power: ")

      //The user gives up the shield or turns back.
      if(a.toLowerCase().equals("y") || a.toLowerCase().equals("yes")) {
        print()
        reader.readLine("Your path is set.")
        reader.readLine("You've gained the power of the warrior.")
        reader.readLine("You've given up the power of the guardian.")
      }
      else {
        print()
        reader.readLine("Take your time.")
        choseStaff()
      }
    }
    //The user goes in some other direction.
    else {
      print("That way is closed off to you for now.")
      choseStaff()
    }
  }
}