/**
  * Created by Jason on 6/5/2017.
  *
  * The purpose of this project is to encrypt words using the Caesar cypher.
  * The cryptographic key will be entered as a command line argument.
  * The program will prompt the user for a word to encrypt using this key.
  */

object Challenge3_Easy {
  val read = scala.io.StdIn
  def main(args: Array[String]) {
    //Because the key never changes, it can be a val instead of a var.
    //The key must be lowercase for the algorithm implemented to work.
    val key = args(0).toLowerCase()

    //Check to see if the key fits the proper format.
    if(goodKey(key)) {

      //The input must be lowercase for the algorithm implemented to work.
      val s: String = read.readLine("Enter the phrase to encrypt: ").toLowerCase()
      //Check to see if the input is a single word with no numbers.
      if(goodFormat(s)) {
        var phrase: String = ""
        for(i <- 0 until s.length()) {
          //Add each encrypted character to the final phrase.
          phrase += encode(s(i), args(0)(0))
        }
        print(phrase)
      }
      else {print("Please enter one entirely alphabetic word at a time.")}
    }
    else {print("Please give a letter to use for the Caesar shift.")}
  }

  //The key should be one alphabetical character.
  def goodKey(k: String): Boolean = {k.matches("[a-z]")}
  //The input should be entirely alphabetical with no numbers, spaces, or special characters.
  def goodFormat(s: String): Boolean = {s.matches("[a-z]+")}

  /* Add the ASCII value of the key and the i-th char of the unencrypted String together.
   * Take modulo 97 of this value so that everything starts from the ASCII value of 'a'
   * Take modulo 26 of this so that values that go past the end of the alphabet wrap back around.
   * Add 97 to this so that the ASCII values line up correctly.
   * Convert this int to a char so that the letter is represented properly.
   */
  def encode(c: Char, k: Char): Char = {((((c.toInt+k.toInt)%97)%26)+97).toChar}
}
